<div class="main-content-wrap">
         <div class="container">
            <div class="services-wrap">
               <div class="header">
                  <h2>OUR SERVICES</h2>
                  <img src="img/divider.png" class="divider-img" alt="html">
                  <h3>Lorem ipsum dolor</h3>
               </div>
               <div class="row inner-service">
                  <div class="col-sm-4 example1">
                     <a href="web-app.html">
                        <div class="img-wrap">
                           <img src="img/webapp.png" alt="web-img">
                        </div>
                        <strong>Web App</strong>
                     </a>
                  </div>
                  <div class="col-sm-4 example1">
                     <a href="design.html">
                        <div class="img-wrap">
                           <img src="img/design.jpg" alt="design-img">
                        </div>
                        <strong>System Design</strong>
                     </a>
                  </div>
                  <div class="col-sm-4 example1">
                     <a href="mobile-app.html">
                        <div class="img-wrap">
                           <img src="img/mobile-app.png" alt="mobile-img">
                        </div>
                        <strong>Mobile App</strong>
                     </a>
                  </div>
               </div>
            </div>
            <div class="portfolio-wrap">
               <div class="header7">
                  <h2>PORTFOLIO</h2>
                  <img src="img/divider.png" class="divider-img" alt="html">
                  <h3>Lorem ipsum dolor</h3>
               </div>
               <div class="row">
                  <!-- <div class="col-sm-2"></div> -->
                  <div class="port-img-wrap">
                     <!-- <div class="col-sm-8"> -->
                        <div class="col-sm-6">
                           <a href="#">
                              <div class="overlay-color">
                                 <div class="content-wrap">
                                    <strong class="hov-title">School</strong>
                                    <div class="sm-img-wrap"><img src="img/attach.png" alt="attach-img"></div>
                                 </div>
                              </div>
                              <div class="hover-img">
                                 <img src="img/school1.jpg" alt="school-img">
                              </div>
                           </a>
                        </div>
                        <div class="col-sm-6">
                           <a href="#">
                              <div class="overlay-color">
                                 <div class="content-wrap">
                                    <strong class="hov-title">Sahakari</strong>
                                    <div class="sm-img-wrap"><img src="img/attach.png" alt="attach-img"></div>
                                 </div>
                              </div>
                              <div class="hover-img">
                                 <img src="img/sahakari.jpg" alt="school-img">
                              </div>
                           </a>
                        </div>
                     <!-- </div> -->
                  </div>
                  <!-- <div class="col-sm-2"></div> -->
               </div>
            </div>
            <div class="testimonial-wrap">
               <div class="header2">
                  <h2>TESTMONIAL </h2>
                  <img src="img/divider.png" class="divider-imgs" alt="html">
                  <h3>Lorem ipsum dolor</h3>
               </div>
               <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
  <!-- Indicators -->
                 <ol class="carousel-indicators">
                   <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                   <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                   <li data-target="#carousel-example-generic" data-slide-to="2"></li>
                 </ol>

                  <!-- profile -->
                  <div class="profile-wrap">
                     <img src="img/profile.jpg" alt="profile-img">
                  </div>
                 <!-- Wrapper for slides -->
                 <div class="carousel-inner" role="listbox">
                   <div class="item active">
                     <img src="img/testimonial-bg.jpg" alt="...">
                     <div class="carousel-caption">
                       <p>
                          Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                       tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                       quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                       consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                       cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                       proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                       </p>
                     </div>
                   </div>
                   <div class="item">
                     <img src="img/testimonial-bg.jpg" alt="...">
                     <div class="carousel-caption">
                       <p>
                          Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                       tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                       quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                       consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                       cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                       proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                       </p>
                     </div>
                   </div>
                 </div>

                 <!-- Controls -->
                 <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                   <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                   <span class="sr-only">Previous</span>
                 </a>
                 <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                   <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                   <span class="sr-only">Next</span>
                 </a>
               </div>


            </div>
         </div>
</div>