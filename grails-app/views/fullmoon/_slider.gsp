<div class="header-banner-wrap">
    <div class="img-wrap">
        <asset:image src="banner-img.jpg" alt="banner-img"/>
        <div class="color-overlay"></div>
    </div>
    <div class="banner-text">
        <strong class="banner-title">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod</strong>
        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea </p>
    </div>
    <ul class="social-media">
        <li><a href="#"><img src="img/linkenden.png" class="imaze" alt="linkenden-img"></a></li>
        <li><a href="#"><img src="img/google.png" class="imaze" alt="google-img"></a></li>
        <li><a href="#"><img src="img/twitter.png" class="imaze" alt="twitter-img"></a></li>
        <li><a href="#"><img src="img/facebook.png" class="imaze" alt="facebook-img"></a></li>
    </ul>

</div>