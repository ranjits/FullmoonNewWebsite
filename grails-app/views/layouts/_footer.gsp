<div class="footer-wrap">
    <div class="header3">
        <h2>CONTACT US </h2>
        <img src="img/divider.png" class="divider-imgger" alt="html">
        <h3>Leave us Message</h3>
    </div>
    <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d3531.256610137311!2d85.3272988!3d27.7402301!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x39eb193a0e9e9d07%3A0xb0b2ef2281624146!2sFull-moon+Information+Technologies!5e0!3m2!1sen!2snp!4v1497589101197" height="300" frameborder="0" style="border:0;width: 100%" allowfullscreen=""></iframe>
    <div class="nav-wrapper">
        <div class="container">
            <div class="row footer">
                <div class="col-sm-6">
                    <h3>Leave us message</h3>
                    <form>
                        <ul>
                            <li>
                                <span class="icon-wrap">
                                    <img src="img/user.png" alt="user">
                                </span>
                                <input type="text" id="fname" name="fname" placeholder="Full Name"/>
                            </li>
                            <li>
                                <span class="icon-wrap">
                                    <img src="img/phone.png" alt="phone">
                                </span>
                                <input type="text" id="phone" name="phone" placeholder="Phone"/>
                            </li>
                            <!-- <li>
                                <span class="icon-wrap">
                                  <img src="img/message.png" alt="message">
                                </span>
                                <input type="text" id="message" name="message" placeholder="Message"/>
                             </li> -->
                            <li>
                                <textarea placeholder="Message"></textarea>
                            </li>
                        </ul>
                        <div class="btn-wrap">
                            <input type="button" class="btn-submit" value="Send">
                        </div>
                    </form>
                </div>
                <div class="col-sm-6">
                    <h3>Fullmoon IT</h3>
                    <h5><i class="fa fa-map-marker" aria-hidden="true"></i>
                        Putalisadak,near Shankardev Campus
                    </h5>
                    <h5><i class="fa fa-phone" aria-hidden="true"></i>
                        +977-9849778601
                    </h5>
                    <h5><span class="glyphicon glyphicon-envelope"></span>
                        fullmoon@the fullmoonit.com
                    </h5>
                    <div class="linker1">
                        <a href="#"><img src="img/facebook.png" class="imazes" alt="facebook-img"></a>
                        <a href="#"><img src="img/twitter.png" class="imazes" alt="twitter-img"></a>
                        <a href="#"><img src="img/google.png" class="imazes" alt="google-img"></a>
                        <a href="#"><img src="img/linkenden.png" class="imazes" alt="linkenden-img"></a>
                    </div>
                </div>
            </div>
        </div>
        <strong class="footer-wrapper">Fullmoon <span>&copy;</span> 2018</strong>
    </div>
</div>

<!-- page finder -->
<script language="JavaScript">
    var sPath=window.location.pathname;
    var sPage = sPath.substring(sPath.lastIndexOf('/') + 1);
    // alert(sPage);
    if(sPage=='index.html'){
        $('body').addClass('indexPage');
    }
</script>
</body>
</html>