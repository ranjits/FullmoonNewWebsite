<div class="header-wrapper">
    <nav class="navbar navbar-default">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">
                <img src="fullmoon-logo.png" alt="logo-img">
            </a>
        </div>
        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav navbar-right">
                <li><a href="#">Home</a></li>
                <li><a href="#">Service</a></li>
                <li><a href="#">Portfolio</a></li>
                <li><a href="#">Testimonial</a></li>
                <!-- <li class="dropdown">
                      <a href="#">About us <span class="caret"></span></a>
                      <ul class="dropdown-menu">
                         <li><a href="#">Title1</a></li>
                         <li role="separator" class="divider"></li>
                         <li><a href="#">Title2</a></li>
                         <li role="separator" class="divider"></li>
                         <li><a href="#">Title3</a></li>
                      </ul>
                   </li> -->
                <li><a href="#">Contact</a></li>
            </ul>
        </div>
        <!-- /.navbar-collapse -->
    </nav>
</div>