<!doctype html>
<html lang="en" class="no-js">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <title>
        Full Moon Information Technologies</title>

    </title>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>

    <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
    <asset:stylesheet href="bootstrap.min.css"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <asset:stylesheet href="fullmoon.css"/>
    <asset:javascript src="jquery-3.2.1.min.js" />
    <asset:javascript src="bootstrap.min.js" />
    <g:layoutHead/>
</head>
<body>

<g:render template="/layouts/header"/>
<g:layoutBody/>
<!--Footer-->
<g:render template="/layouts/footer"/>
<!-- Footer end -->

</body>
</html>
